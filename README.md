Pomodoro
===

Cli pomodoro application

**Note** All times are in seconds

## Usage
### Start pomodoro (aka punch-in)
```bash
pomodoro start
```
or
```bash
pomodoro start --work 600 --pause 60
```

### Halt timer
```bash
pomodoro halt
```

### Stop timer (aka punch-out)
```bash
pomodoro halt
```


## Bonus features

### Snooze timer
```bash
pomodoro snooze
```
Or with time
```bash
pomodoro snooze 60
```
### Schedule based pomodoro
Attempt at breaking all workable time into small chunks while taking meetings into account
Is incompatible with both --work & --pause (unless you keep spliting untill you get a meeting)

```mermaid
graph LR;
subgraph clock{
  tick;
  schedule:;
  push_to_schedule:;
  
  tick -- consult --> schedule;
  push_to_schedule: --> schedule;
  control -- start/halt/stop--> tick;
  control -- send event --> plugins
  tick -- send event --> plugins
}

cli;

cli -- uses --> control;

plugin-time-chunk
plugin-time-chunk -- hook to --> plugins;
plugin-time-chunk --> push_to_schedule;

plugin-calendar;
plugin-calendar -- hook to --> plugins;
plugin-calendar --> push_to_schedule;
```

TODO
===
1. Offload the tick function to a service/subprocess (as long as we can call it via cli)
1. Move arguments to a cli module/package
1. Control the service via the cli (how to communicate? socket maybe? Is there a event system for rust?)
1. Offload the time chunk logic to a data store (inmemory or file)
1. Abstract schedule data store access (push_to_schedule)
  * Start with push to schedule, but we should add more actions later
  * Should the halt actions be added to the schedule? (treat it as an event system)
1. Use schedule to create a classic pomodoro schedule
