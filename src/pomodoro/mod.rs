use notify_rust::Notification;
use std::time::Duration;
use tokio::prelude::*;
use tokio::timer::Interval;

pub fn start(work_time_seconds: u32, pause_time_seconds: u32) {
    let app_name = "Pomodoro";
    let mut tick = 0;
    let interval = 1000; // 1 second
    let mut is_working = true;
    let task = Interval::new_interval(Duration::from_millis(interval))
        .for_each(move |_| {
            tick = tick + 1;

            if is_working == true && tick == work_time_seconds {
                is_working = false;
                tick = 0;
                notify(&app_name, &format!("{} seconds elapsed, time to take a break!", work_time_seconds));
                println!("{} seconds elapsed, time to take a break!", work_time_seconds);
            } else if is_working == false && tick == pause_time_seconds {
                is_working = true;
                tick = 0;
                notify(&app_name, &format!("{} seconds elapsed, time to go back to work!", pause_time_seconds));
                println!("{} seconds elapsed, time to go back to work!", pause_time_seconds);
            }

            Ok(())
        })
        .map_err(|e| panic!("interval errored; err={:?}", e));

    tokio::run(task);
}


fn notify(summary: &str, message: &str) {
    Notification::new()
        .summary(&summary)
        .body(message)
        .show()
        .unwrap();
}

