use structopt::StructOpt;

mod pomodoro;

#[derive(StructOpt)]
struct Cli {
    #[structopt(short = "w", long = "work", min_values = 60, default_value = "1500")]
    work: u32,
    #[structopt(short = "p", long = "pause", min_values = 60, default_value = "300")]
    pause: u32,
}

fn main() {
    let args = Cli::from_args();

    pomodoro::start(args.work, args.pause);
}
